
import numpy as np 

import theano
import theano.tensor as T

from theano.tensor.signal import downsample
from theano.tensor.nnet import conv

import matplotlib.pyplot as plt

import time
from collections import OrderedDict


def readTrainingData(path = '../Data/GTSRB/', inclRotated = False, inclGamma = False):

	#load labels
	pathLabels = path + 'ImagesResized/labels.csv'
	labels = np.genfromtxt(pathLabels, delimiter = ',')

	images = []

	# load resized images
	for i in range(len(labels)):
		pathImage = path + 'ImagesResized/' + format(i, '06d') + '.ppm'
		images.append(plt.imread(pathImage))


	# if desired, load rotated versions of images
	if inclRotated:

		pathLabelsRot = path + 'ImagesRotated/labels_rot.csv'
		labelsRot = np.genfromtxt(pathLabelsRot, delimiter = ',')

		imagesRot = []

		for i in range(len(labelsRot)):
			pathImageRot = path + 'ImagesRotated/' + format(i, '06d') + '_rot.ppm'
			imagesRot.append(plt.imread(pathImageRot))

		labels = np.concatenate((labels,labelsRot)) # labels is already a numpy array
		images = images + imagesRot # images are still list of numpy arrays

	# if desired, load images where the greyscale was calculated with different weights
	if inclGamma:

		pathLabelsGa = path + 'ImagesGamma/labels_ga.csv'
		labelsGa = np.genfromtxt(pathLabelsGa, delimiter = ',')

		imagesGa = []

		for i in range(len(labelsGa)):
			pathImageGa = path + 'ImagesGamma/' + format(i, '06d') + '.ppm'
			imagesGa.append(plt.imread(pathImageGa))

		labels = np.concatenate((labels,labelsGa)) # labels is already a numpy array
		images = images + imagesGa # images are still list of numpy arrays

	return np.asarray(images), labels

def readTestData(path = '../Data/GTSRB/'):

	#load labels
	pathLabels = path + 'TestResized/labels.csv'
	labels = np.genfromtxt(pathLabels, delimiter = ',')

	images = []

	# load resized images
	for i in range(len(labels)):
		pathImage = path + 'TestResized/' + format(i, '06d') + '.ppm'
		images.append(plt.imread(pathImage))

	return images, labels

def ReLu(x):
	return T.maximum(0.,x)


def calcShape(pixelsIn, widthConv = 1, widthPool = 1):

	if ((pixelsIn - widthConv + 1) % widthPool) != 0:
		print "ERROR: calculated shape not an int"

	pixelsOut = ((pixelsIn - widthConv + 1) / float(widthPool))

	return int(pixelsOut)

def dropoutLayer(x, train, p = 0.5):
    # drops each value of x with probability p
    if train:
        seed = np.random.randint(2 ** 30)
        srng = theano.tensor.shared_randomstreams.RandomStreams(seed)
        mask = srng.binomial(n=1, p=1.-p, size=x.shape,
                dtype=theano.config.floatX)
        return x * mask
    # if evaluation phase, the weights are averaged
    else:
    	return x * p

class ConvNet(object):
	# class for the Convolutional Network

	def __init__(self, data, nrLabels, layers, batchSize, imageShape, dropout = False, train = False):

		self.layersOutput = []
		self.layersShapes = []
		self.params = []

		# first layer is always the input:
		input_shape = (batchSize, 1, imageShape[0], imageShape[1])
		self.layersShapes.append(input_shape)

		data = data.reshape(input_shape)
		self.layersOutput.append(data)

		print "builds following CNN:"

		# construct the net by adding Convolution Layers, MaxPool Layers and MLP Layers
		for currentLayer in layers:

			# get output and shape of last layer
			inputToLayer = self.layersOutput[-1]
			inputToLayer_shape = self.layersShapes[-1]

			print "    Shape of input to next layer:", inputToLayer_shape

			if len(currentLayer) == 3:
				# ConvLayer where current layer is (nKerns, widthKern, heightKern)

				print "  Layer: Convolution with filter shape:", currentLayer

				# has to be on top of input or other convolutional layer
				assert len(inputToLayer_shape) == 4, "ERROR: shape of previous Layer does not fit convolution"
				
				# get params for layer
				nKerns = currentLayer[0]
				widthKern = currentLayer[1]
				heightKern = currentLayer[2]
				channelsImage = inputToLayer_shape[1]
				widthImage = inputToLayer_shape[2]
				heightImage = inputToLayer_shape[3]

				# initialise weights for this layer
				filterShape = (nKerns, channelsImage, widthKern, heightKern)
				weights = theano.shared(np.random.normal(0, 0.01, (filterShape)))
				bias = theano.shared(np.random.normal(0,0.1, (filterShape[0])))

				# construct layer
				outputConv = conv.conv2d(
					input = inputToLayer,
					filters = weights,
					filter_shape = filterShape,
					image_shape = inputToLayer_shape)

				output = ReLu(outputConv + bias.dimshuffle('x', 0, 'x', 'x'))
				output_shape = (inputToLayer_shape[0], # batchSize remains the same
								nKerns, # nr of OutputChannels is nrKerns
								calcShape(widthImage, widthConv = widthKern), # new image width
								calcShape(heightImage, widthConv = heightKern)) # new image height

				# add layer to net
				self.params = self.params + [weights, bias]
				self.layersOutput.append(output)
				self.layersShapes.append(output_shape)


			elif len(currentLayer) == 2:
				# maxPool Layer where currentLayer is (widthPool, heightPool)

				print "  Layer: MaxPool with filter shape:", currentLayer

				# has to be after convolutional layer
				assert len(inputToLayer_shape) == 4, "ERROR: shape of previous Layer does not fit MaxPool"

				# construct maxPool Layer
				output = downsample.max_pool_2d(
					input = inputToLayer,
					ds = currentLayer,
					ignore_border = True)

				output_shape = (inputToLayer_shape[0], # batchSize unchanged
								inputToLayer_shape[1], # nKerns unchanged
								inputToLayer_shape[2] / currentLayer[0], # new Image width
								inputToLayer_shape[3] / currentLayer[1]) # new Image height

				# add maxPool Layer to Net
				self.layersOutput.append(output)
				self.layersShapes.append(output_shape)

			elif len(currentLayer) == 1:
				# fully connected layer

				print "  Layer: Hidden with nrNeurons:", currentLayer

				if (len(inputToLayer_shape) == 4):
					# last layer was Conv or MaxPool --> flatten input
					inputToLayer = inputToLayer.flatten(2)
					nrNeurons_in = inputToLayer_shape[1] * inputToLayer_shape[2] * inputToLayer_shape[3]
					inputToLayer_shape = (inputToLayer_shape[0], nrNeurons_in)

				elif (len(inputToLayer_shape) == 2):
					# last layer was fully connected --> no need to flatten input
					nrNeurons_in = inputToLayer_shape[1]

				else:
					raise AssertionError("ERROR: shape of previous Layer does not fit MLP")

				nrNeurons_out = currentLayer[0]

				# initialise weights of layer
				weights = theano.shared(np.random.normal(0, 0.01, (nrNeurons_in, nrNeurons_out)))
				bias = theano.shared(np.random.normal(0, 0.01, (nrNeurons_out)))

				# construct layer
				output = T.tanh(T.dot(inputToLayer, weights) + bias)
				output_shape = (inputToLayer_shape[0], nrNeurons_out)

				# add layer to net
				self.params = self.params + [weights, bias]
				self.layersOutput.append(output)
				self.layersShapes.append(output_shape)


			else:
				raise AssertionError ("ERROR: type of layer not recognized")


		# Last layer is always as softmax Layer for Classification
		inputSoftmax = self.layersOutput[-1]
		inputSoftmax_shape = self.layersShapes[-1]

		print "  Layer: Softmax"

		if (len(inputSoftmax_shape) == 4):
			# last layer was Conv or MaxPool --> flatten input
			inputSoftmax = inputSoftmax.flatten(2)
			inputSoftmax_dim = inputSoftmax_shape[1] * inputSoftmax_shape[2] * inputSoftmax_shape[3]

		elif (len(inputSoftmax_shape) == 2):
			# last layer was fully connected --> no need to flatten input
			inputSoftmax_dim = inputSoftmax_shape[1]

		else:
			raise AssertionError("ERROR: shape of previous Layer does not fit Softmax")

		# add dropout to Softmax input
		if dropout:
			inputSoftmax = dropoutLayer(inputSoftmax, train)

		# initialise weights of softmax
		weights = theano.shared(np.random.normal(0, 0.01, (inputSoftmax_dim, nrLabels)))
		bias = theano.shared(np.random.normal(0, 0.01, (nrLabels)))


		# output of classification
		self.outputSoftmax = T.nnet.softmax(T.dot(inputSoftmax, weights) + bias)
		self.pred = T.argmax(self.outputSoftmax, axis = 1)

		# add params to net
		self.params = self.params + [weights, bias]


	def cost_mean(self,y):
		# mean error

		return -T.mean(T.log(self.outputSoftmax)[T.arange(y.shape[0]), y])

	def cost_sum(self,y):
		# error sum

		return -T.sum(T.log(self.outputSoftmax)[T.arange(y.shape[0]), y])

	def errors(self,y):
		# returns percentage of correctly classified images

		return T.mean(T.neq(self.pred, y))


def trainModel(images, labels, nrLabels, layers, batchSize = 100, lRate = 0.01, momentum = 0.7,
				trainMethod = 'SGD', seed = 1234, maxEpochs = 30, dropout = False):
	# defines the model and trains it, using the ConvNet class

	np.random.seed(seed)

	nrExamples = len(images)

	# shuffle data twice, because it was ordered before
	for i in range(2):
		indices = np.random.permutation(nrExamples)
		images, labels = images[indices], labels[indices]

	# if last batch is not full, it will be dropped
	nrBatches = nrExamples // batchSize
	nrExamples = nrBatches * batchSize

	# 10% of all batches are for validation
	nrTrainExamples = int(nrBatches*0.9) * batchSize

	train_x, train_y = images[:nrTrainExamples], labels[:nrTrainExamples]
	valid_x, valid_y = images[nrTrainExamples : nrExamples], labels[nrTrainExamples : nrExamples]

	nrTrainBatches = len(train_x) // batchSize
	nrValidBatches = len(valid_x) // batchSize


	print nrTrainBatches, "batches for training"
	print nrValidBatches, "batches for validation"
	print "size of batch:", batchSize

	#### build training graph ####

	# initialise symbolic variables
	x = T.tensor3('x') # for batchsize X imageWidht X imageHeight
	y = T.ivector('y')
	learning_rate = T.dscalar('learning_rate')
	index = T.lscalar('index')

	imageShape = images[0].shape

	# build net
	CNN = ConvNet(
		data = x,
		nrLabels = nrLabels,
		layers = layers,
		batchSize = batchSize,
		imageShape = imageShape,
		dropout = dropout,
		train = True
		)

	# get gradients
	cost = CNN.cost_mean(y)
	grads = T.grad(cost, CNN.params)


	# define updates
	if trainMethod == 'SGD':
		updates = []
		for parameter, gradient in zip(CNN.params, grads):
			updates.append((parameter, parameter - learning_rate * gradient))

	elif trainMethod == 'SGD_Momentum':
		updates = []
		for parameter, gradient in zip(CNN.params, grads):
			parameter_update = theano.shared(parameter.get_value() * 0.)
			updates.append((parameter, parameter - parameter_update * learning_rate))
			updates.append((parameter_update, parameter_update * momentum + (1. - momentum) * gradient))
	else: print "ERROR: unrecognized training method"


		
	print "training with", trainMethod

	# store dataset as shared variables to copy all to GPU instead of recopying for every epoch
	# cast to int needed for labels because on the GPU everything is stored as a float
	train_x = theano.shared(np.asarray(train_x, dtype=theano.config.floatX), name='train_x')
	train_y = T.cast(theano.shared(np.asarray(train_y, dtype=theano.config.floatX), name='train_y'), 'int32')  
	valid_x = theano.shared(np.asarray(valid_x, dtype=theano.config.floatX), name='valid_x') 
	valid_y = T.cast(theano.shared(np.asarray(valid_y, dtype=theano.config.floatX), name='valid_y'), 'int32')


	# compile functions
	train = theano.function(
		inputs =[index, learning_rate],
		outputs = CNN.errors(y),
		updates = updates,
		givens = {
				x: train_x[index * batchSize: (index + 1) * batchSize],
				y: train_y[index * batchSize: (index + 1) * batchSize]
				}
		)

	validate = theano.function(
		inputs =[index],
		outputs = CNN.errors(y),
		givens = {
				x: valid_x[index * batchSize: (index + 1) * batchSize],
				y: valid_y[index * batchSize: (index + 1) * batchSize]
				}
		)


	### actual training ###

	training_start = time.clock()

	print "start training"

	bestEpoch = 0
	bestError = 100.

	for epoch in range(maxEpochs):

		epoch_start = time.clock()

		errorTrainEpoch = []
		errorValidEpoch = []

		for batchIndex in range(nrTrainBatches):
			errorTrain = train(batchIndex, lRate)
			errorTrainEpoch.append(errorTrain)

		for batchIndex in range(nrValidBatches):
			errorValid = validate(batchIndex)
			errorValidEpoch.append(errorValid)

		errorValidEpoch = np.mean(errorValidEpoch)
		errorTrainEpoch = np.mean(errorTrainEpoch)

		print ("%f validation error, %f training error after %d epochs" % 
			(errorValidEpoch, errorTrainEpoch, epoch + 1))

		if (bestError > errorValidEpoch):
			bestError = errorValidEpoch
			bestEpoch = epoch + 1

		# annealing of learning Rate
		if epoch > maxEpochs / 2 - 1:
			lRate = lRate / 2

	print "training took %f mins" % ((time.clock() - training_start) / 60)

	parameterStorage = []

	for parameter in CNN.params:
		parameterStorage.append(parameter.get_value())

	return parameterStorage, errorValidEpoch, bestError, bestEpoch

def evaluateModel(images, labels, params, nrLabels, layers, dropout = False):

	x = T.tensor3('x')
	y = T.ivector('y')

	batchSize = len(images)
	imageShape = images[0].shape

	CNN = ConvNet(
		data = x,
		nrLabels = nrLabels,
		layers = layers,
		batchSize = batchSize,
		imageShape = imageShape,
		dropout = dropout,
		train = False
		)

	for paramsTrain, paramsPredict in zip(params, CNN.params):
		paramsPredict.set_value(paramsTrain)

	predict = theano.function([x], CNN.errors(labels))

	testError = predict(images)

	print "%f is the final test error" % (testError)

	return testError


def main():

	print "read in Data"

	inclRotated = True # include rotated version of images
	inclGamma = True # include version of images where the grayscale was calculated with diff weights
	images, labels = readTrainingData(inclRotated = inclRotated, inclGamma = inclGamma)
	imagesTest, labelsTest = readTestData()
	nrLabels = 43

	# params
	seed = 6000
	lRate = 0.015 # for the second half of epochs, lRate will be halved at the end of each one
	maxEpochs = 15
	momentum = 0.9
	trainMethod = 'SGD_Momentum' 
	batchSize = 100
	dropout = True # adds dropout before the softmax layer
	layers = [(50,5,5), (50,5,5), (2,2), (80,5,5), (2,2), (400,)] 
	# defines the layers of the network
	# 3-tuple --> ConvLayer + ReLu with (nrKernels, widthKernel, heightKernel)
	# 2-tuple --> maxPoolLayer with (widthPool, heightPool)
	# 1-tuple --> fully connected Layer with (nrNeurons) (Important to be tuple and not int)

	print "start training"

	params, errorValid, errorValidBest, epochBest = trainModel(
		images = images,
		labels = labels,
		nrLabels = nrLabels,
		lRate = lRate,
		trainMethod = trainMethod,
		layers = layers,
		batchSize = batchSize,
		seed = seed,
		maxEpochs = maxEpochs,
		momentum = momentum,
		dropout = dropout)

	print "start evaluation"

	errorTest = evaluateModel(
		images = imagesTest,
		labels = labelsTest,
		nrLabels = nrLabels,
		layers = layers,
		params = params,
		dropout = dropout)

	# save results and parameters to a log file
	logTxt = "\ntest Error: "+str(errorTest)+ "\nValidation Error: "+str(errorValid)+ "\nEpochs: " + str(maxEpochs)+ "\nbest Validation Error: "+str(errorValidBest)+"\tin Epoch: "+str(epochBest)+"\ninclRotated: "+str(inclRotated)+"\t inclGamma: "+str(inclGamma)+"\nLayers: "+str(layers)+ "\ntraining Method: "+trainMethod+"\nDropout: "+str(dropout)+"\nbatch size: "+str(batchSize)+"\nlearning Rate: " + str(lRate) + "\nMomentum: "+str(momentum)+"\nAnnealing: after half of epochs, half lRate with every epoch"+"\n"

	with open("../log.txt", "a") as log:
		log.write(logTxt)
	log.close()


if __name__ == '__main__':

	main()

'''
#### Example Result from log File ####

test Error: 0.0391132224861
Validation Error: 0.00313559322034
Epochs: 15
best Validation Error: 0.00313559322034	in Epoch: 10
inclRotated: True	 inclGamma: True
Layers: [(50, 5, 5), (50, 5, 5), (2, 2), (80, 5, 5), (2, 2), (400,)]
training Method: SGD_Momentum
Dropout: True
batch size: 100
learning Rate: 0.015
Momentum: 0.9	rho: 0.01	eps: 0.9
Annealing: after half of epochs, half lRate with every epoch
'''
